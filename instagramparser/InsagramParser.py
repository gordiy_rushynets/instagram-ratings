import os
import time
import json
import random
import smtplib
import xlsxwriter
from .InstagramAPI import InstagramAPI
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders


class InstagramParser:
    __user_ids = []
    __API = None
    __MY_ADDRESS = "gordiy.yuriy.gord123@gmail.com"
    __PASSWORD = "46256As61F2"

    def __init__(self, proxy=None, **userData):
        if userData:
            self.__API = InstagramAPI(username=userData['username'], password=userData['password'])
            if proxy is not None:
                self.__API.setProxy(userData['username']+':'+userData['password']+'@'+proxy)
                print("Login with proxy")
            else:
                self.__API.login()
                print("Login without proxy")

    def get_user_data(self, username):
        self.__API.searchUsername(username)
        username = self.__API.LastJson['user']['username']
        full_name = self.__API.LastJson['user']['full_name']
        try:
            profile_pic_versions320 = self.__API.LastJson['user']['hd_profile_pic_versions'][0]['url']
        except Exception as e:
            profile_pic_versions320 = ''
            print("profile_pic_versions320", e)
        try:
            profile_pic_versions640 = self.__API.LastJson['user']['hd_profile_pic_versions'][1]['url']
        except Exception as e:
            profile_pic_versions640 = ''
            print("profile_pic_versions640 exception:", e)
        try:
            profile_pic_versions1080 = self.__API.LastJson['user']['hd_profile_pic_url_info']['url']
        except Exception as e:
            profile_pic_versions1080 = ''
            print("profile_pic_versions1080 exception:", e)
        media_count = self.__API.LastJson['user']['media_count']
        follower_count = self.__API.LastJson['user']['follower_count']
        following_count = self.__API.LastJson['user']['following_count']
        biography = self.__API.LastJson['user']['biography']
        data = {
            'username': username,
            'full_name': full_name,
            'biography': biography,
            'profile_pic_version320': profile_pic_versions320,
            'profile_pic_version640': profile_pic_versions640,
            'profile_pic_version1080': profile_pic_versions1080,
            'media_count': media_count,
            'follower_count': follower_count,
            'following_count': following_count
        }
        return data

    def get_user_id_by_username(self, username):
        self.__API.searchUsername(username)
        random_sleep = random.randint(1, 3)
        time.sleep(random_sleep)

        user_id = None

        if self.__API.LastJson['status'] == 'ok':
            user_id = self.__API.LastJson['user']['pk']
        else:
            print("Can not get user id, username:", username)

        return user_id

    def get_count_likes_and_comments(self, user_id):
        self.__API.getUserFeed(usernameId=user_id)

        if 'next_max_id' in self.__API.LastJson:
            next_max_id = self.__API.LastJson['next_max_id']
            big_list = True
        else:
            big_list = False

        posts_information = []

        try:
            for i in range(len(self.__API.LastJson['items'])):
                info = {}

                like_count = self.__API.LastJson['items'][i]['like_count']
                try:
                    comment_count = self.__API.LastJson['items'][i]['comment_count']
                except KeyError:
                    print("Comment disable")
                profile_pic_url = self.__API.LastJson['items'][i]['user']['profile_pic_url']
                try:
                    post_url = self.__API.LastJson['items'][i]['image_versions2']['candidates'][0]['url']
                except KeyError:
                    try:
                        post_url = self.__API.LastJson['items'][i]['carousel_media'][0]['image_versions2']['candidates'][0]['url']
                    except KeyError:
                        pass

                info = {"like_count": like_count, "comment_count": comment_count, "profile_pic_url": profile_pic_url, "post_url": post_url}
                posts_information.append(info)

        except KeyError:
            print("Haven't posts")

        while big_list:
            self.__API.getUserFeed(usernameId=user_id, maxid=next_max_id)
            s = random.randint(0, 2)
            print("LIKES:", s)
            time.sleep(s)

            for i in range(len(self.__API.LastJson['items'])):
                info = {}

                like_count = self.__API.LastJson['items'][i]['like_count']
                try:
                    comment_count = self.__API.LastJson['items'][i]['comment_count']
                except KeyError:
                    print("Comment disable")
                profile_pic_url = self.__API.LastJson['items'][i]['user']['profile_pic_url']
                try:
                    post_url = self.__API.LastJson['items'][i]['image_versions2']['candidates'][0]['url']
                except KeyError:
                    try:
                        post_url = self.__API.LastJson['items'][i]['carousel_media'][0]['image_versions2']['candidates'][0]['url']
                    except KeyError:
                        pass

                info = {"like_count": like_count, "comment_count": comment_count, "profile_pic_url": profile_pic_url,
                        "post_url": post_url}
                posts_information.append(info)

            try:
                next_max_id = self.__API.LastJson['next_max_id']
                big_list = True
            except KeyError:
                big_list = False

        return posts_information

    def get_count_followers(self, username):
        self.__API.searchUsername(username)
        count_followers = self.__API.LastJson['user']['follower_count']
        return count_followers

    def get_user_posts_date(self, user_id, date_stop=None):
        # get user's posts by user_id
        global stop_parsing

        # {"date of creating post": "text in post"}
        info = {}

        big_list = True

        # get all posts with pagination
        while big_list:
            self.__API.getUserFeed(usernameId=user_id, maxid=next_max_id)
            s = random.randint(0, 3)
            time.sleep(s)
            try:
                for i in range(len(self.__API.LastJson['items'])):
                    stop_parsing = False
                    if date_stop is not None:
                        # date of creating post
                        try:
                            created = self.__API.LastJson['items'][i]['caption']['created_at']
                            # set range for parsing
                            if time.time() - date_stop <= created <= time.time():
                                print("created =", created)
                                info[created] = self.__API.LastJson['items'][i]['caption']['text']
                            else:
                                stop_parsing = True
                                break
                        except TypeError:
                            pass
                    else:
                        try:
                            created = self.__API.LastJson['items'][i]['caption']['created_at']
                            info[created] = self.__API.LastJson['items'][i]['caption']['text']
                        except TypeError:
                            try:
                                info['00000000000'] = self.__API.LastJson['items'][i]['caption']['text']
                            except TypeError:
                                print("Haven't description")
                            except KeyError:
                                print("Key error in text")
                        except KeyError:
                            print(self.__API.LastJson)
            except KeyError:
                print("Haven't posts")

            try:
                next_max_id = self.__API.LastJson['next_max_id']
                if stop_parsing:
                    big_list = False
                else:
                    big_list = True

            except KeyError:
                big_list = False

        return info

    def get_user_posts(self, user_id):
        """
        function must return information about all user's posts

        :param user_id:
        :return: {'user': {'user_id': user_id, 'username': username, 'full_name': full_name}, 'items':[{
            'post_photo_url': post_photo_url,
            'like_count': like_count,
            'comment_count': comment_count,
            'text': text,
            'created_at': created_at
        }]}
        """

        big_list = True
        posts = []
        while big_list:
            self.__API.getUserFeed(usernameId=user_id)

            if 'next_max_id' in self.__API.LastJson:
                next_max_id = self.__API.LastJson['next_max_id']
                if next_max_id is not None:
                    big_list = True
            else:
                big_list = False

            try:
                username = self.__API.LastJson['items'][0]['user']['username']
                full_name = self.__API.LastJson['items'][0]['user']['full_name']
                for i in range(len(self.__API.LastJson['items'])):
                    info = {}

                    # get like count in post
                    info['like_count'] = self.__API.LastJson['items'][i]['like_count']

                    # get post_photo url in post
                    try:
                        info['post_photo_url'] = self.__API.LastJson['items'][i]['image_versions2']['candidates'][0]['url']
                    except KeyError:
                        # get first photo in carousel media into post
                        info['post_photo_url'] = self.__API.LastJson['items'][i]['carousel_media'][0]['image_versions2']['candidates'][0]['url']

                    # get comments count
                    try:
                        info['comment_count'] = self.__API.LastJson['items'][i]['comment_count']
                    except KeyError:
                        # even if comments count is not able for parsing
                        info['comment_count'] = 0

                    # get description of the post
                    try:
                        info['text'] = self.__API.LastJson['items'][i]['caption']['text']
                    except TypeError:
                        info['text'] = ''
                    except KeyError:
                        info['text'] = ' '

                    # get date of creating post
                    try:
                        info['created_at'] = self.__API.LastJson['items'][i]['caption']['created_at']
                    except TypeError:
                        info['created_at'] = None
                    except KeyError:
                        info['created_at'] = None

                    # set data to list
                    posts.append(info)

            except Exception as e:
                print("Exeption", e)

        try:
            data = {"user": {"user_id": user_id, "username": username, "full_name": full_name}, "items": posts}
        except:
            data = {"user": {"user_id": user_id},"items": posts}

        return data

    def get_user_followers(self, user_id):
        self.__API.getUserFollowers(usernameId=user_id)
        next_max_id = None
        big_list = False

        users = []
        # check if profile is not private
        for i in range(len(self.__API.LastJson['users'])):
            self.__user_ids.append(self.__API.LastJson['users'][i]['pk'])
            users.append(self.__API.LastJson['users'][i]['pk'])

        try:
            if self.__API.LastJson['next_max_id'] is not None:
                next_max_id = self.__API.LastJson['next_max_id']
                big_list = True
        except KeyError:
            big_list = False

        time.sleep(1)
        while big_list:
            self.__API.getUserFollowers(usernameId=user_id, maxid=next_max_id)
            s = random.randint(1, 4)
            time.sleep(s)

            for i in range(len(self.__API.LastJson['users'])):
                try:
                    self.__user_ids.append(self.__API.LastJson['users'][i]['pk'])
                    users.append(self.__API.LastJson['users'][i]['pk'])
                except KeyError:
                    print("Key error!")

            try:
                if self.__API.LastJson['next_max_id'] is not None:
                    next_max_id = self.__API.LastJson['next_max_id']
                    big_list = True
                else:
                    big_list = False
            except KeyError:
                big_list = False


        return users

    def get_hashed_user(self, username, term):
        # set path for hashing data about user
        directory_users = os.getcwd() + '/users'
        # file with users data
        try:
            stat = os.stat(directory_users + '/' + username + '.json')
            date_modification = stat.st_mtime

            # read data from file
            if time.time() - term <= date_modification:
                with open(directory_users + '/' + username + '.json') as json_file:
                    data = json.load(json_file)

                return data
            else:
                return None
        except FileNotFoundError:
            return None

    def hash_user(self, username, user_data):
        directory_users = os.getcwd() + '/' + 'users'

        with open(directory_users + '/' + username + '.json', 'w') as json_file:
            json.dump(user_data, json_file)

    def genetate_exel_file(self, username, user_data):
        # Workbook is created
        path_to_file = os.getcwd() + '/user_xlsx/' + username + '.xlsx'
        workbook = xlsxwriter.Workbook(path_to_file)

        # add_sheet is used to create sheet.
        sheet1 = workbook.add_worksheet()
        # str, col
        sheet1.write("A1", 'Username')
        sheet1.write("B1", 'Top Words')
        sheet1.write("C1", 'Count')
        sheet1.write("D1", 'Top Hashtags')
        sheet1.write("E1", 'Count')

        sheet1.write("A2", username)

        print(user_data['data'])
        top_words = user_data['data']['top_words']
        top_hashtags = user_data['data']['top_hashtags']
        i = 2
        for word, num in top_words.items():
            sheet1.write("B"+str(i), word)
            sheet1.write("C"+str(i), num)
            i += 1

        i = 2
        for hashtag, num in top_hashtags.items():
            sheet1.write("D"+str(i), hashtag)
            sheet1.write("E"+str(i), num)
            i += 1

        del i

        workbook.close()

    def send_exel_file_by_email(self, username, email_to):
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        s.login(self.__MY_ADDRESS, self.__PASSWORD)

        msg = MIMEMultipart()  # create a message

        msg['From'] = self.__MY_ADDRESS
        msg['To'] = email_to
        msg['Subject'] = "IGParser"
        try:
            filename = os.getcwd() + '/user_xlsx/' + username + '.xlsx'
            attachment = open(filename, 'rb')
        except FileNotFoundError:
            filename = os.getcwd() + '/user_xlsx/' + username + '.xls'
            attachment = open(filename, 'rb')

        # instance of MIMEBase and named as p
        p = MIMEBase('application', 'octet-stream')

        # To change the payload into encoded form
        p.set_payload((attachment).read())

        # encode into base64
        encoders.encode_base64(p)

        p.add_header('Content-Disposition', "attachment; filename=%s" % username + ".xlsx")

        # attach the instance 'p' to instance 'msg'
        msg.attach(p)

        # send the message via the server set up earlier.
        s.send_message(msg)
        print(msg)

        s.quit()

    def get_user_ids(self):
        return self.__user_ids