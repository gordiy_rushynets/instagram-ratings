import emoji
import regex
from .InsagramParser import InstagramParser
from .local_settings import login, pswd

prepositions_ru = ["на", "На", "З",
                       "Из", "з", "из",
                       "по", "По", "Под",
                       "под", "К", "к",
                       "Об", "об", "От",
                       "от", "В", "в",
                       "у", "У", "С",
                       "с", "о", "О",
                       "Над", "над", "Около",
                       "около", "при", "При",
                       "Перед", "перед", "Но",
                       "но"]
prepositions_en = ["In", "in", "On",
                   "on", "at", "At",
                   "For", "From", "for",
                   "from", "To", "to"]

characters = ["-", ".", ":", ",", ";", "?", "...", "!", " "]

top_words = {}
top_hashtag = {}

def get_hashtags(str):
    list_hastags = str.split("#")

    hashtags = []

    for word in list_hastags:
        if len(word) > 0:
            if word[len(word) - 1] == " ":
                word = word[:len(word) - 1]
            elif word[len(word) - 1] in characters:
                word = word[:len(word) - 1]
            hashtags.append(word)

    return hashtags

def check_last_character(str):
    if len(str) > 1:
        try:
            while str[len(str) - 1] in characters:
                str = str[:len(str) - 1]
        except IndexError:
            pass
    return str

def remove_emoji(text):
    data = regex.findall(r'\X', text)
    for word in data:
        if any(char in emoji.UNICODE_EMOJI for char in word):
            # Remove from the given text the emojis
            text = text.replace(word, '')
    return text

def remove_numbers(text):
    # check if time
    try:
        if text[2] == ":":
            return text
        elif text[2] == "." and text[5] == ".":
            return text
        elif text.isdigit():
            return ''
        else:
            for i in range(len(text)):
                if text[i].isalpha():
                    if i >= 2:
                        return text
                else:
                    return ''
    except IndexError:
        return text

def get_top_words(parser, user_id, date_stop = None):
    # get top description of the posts
    if isinstance(date_stop, int) or isinstance(date_stop, float):
        data_posts = parser.get_user_posts_date(user_id, date_stop)
    else:
        data_posts = parser.get_user_posts_date(user_id)

    for key, value in data_posts.items():
        value = remove_emoji(value)
        list_words = value.split()


        for i in range(len(list_words)):
            if list_words[i] not in prepositions_en and list_words[i] not in prepositions_ru and list_words[i] not in characters:
                # if word is in top_words dictionary
                if list_words[i] in top_words and list_words[i][0] != "#":
                    # check even if last elem is token
                    try:
                        # remove emoji from text
                        text = remove_emoji(list_words[i])
                        # remove numbers
                        text = remove_numbers(text)
                        if len(text) > 1:
                            top_words[check_last_character(text)] += 1
                    except KeyError:
                        pass
                    print(text, "-", top_words[list_words[i]])
                # if word is not in top_words dict
                elif list_words[i] not in top_words and list_words[i][0] != "#":
                    # remove emoji from text
                    text = remove_emoji(list_words[i])
                    # remove numbers
                    text = remove_numbers(text)
                    top_words[check_last_character(text)] = 1
                # if word is hashtag
                elif list_words[i][0] == "#":
                    hashtags = get_hashtags(list_words[i])
                    # fill hashtag in top_hastag dict
                    for j in range(len(hashtags)):
                        if hashtags[j] in top_hashtag:
                            top_hashtag[hashtags[j]] += 1
                            print(hashtags[j], "-", top_hashtag[hashtags[j]])
                        else:
                            top_hashtag[hashtags[j]] = 1

def person_data(parser, username):

    data = parser.get_user_data(username)
    return data

def person_rating(parser, username):
    # print("USERNAME =", username)
    user_id = parser.get_user_id_by_username(username)
    count_followers = parser.get_count_followers(username)

    likes = parser.get_count_likes_and_comments(user_id)

    count_comments = 0
    count_likes = 0

    for i in likes:
        count_comments += i['comment_count']
        count_likes += i['like_count']

    return {"username": username, "count_followers": count_followers, "count_likes": count_likes, "count_comments": count_comments}

def get_user_posts(parser, username):
    data = None
    user_id = parser.get_user_id_by_username(username)
    if user_id is not None:
        data = parser.get_user_posts(user_id=user_id)
    else:
        print("Can't get user's posts.")
    return data

def create_parser(login, pswd):
    parser = InstagramParser(username=login, password=pswd)
    return parser

# user_data = {"username": {"user_id": user_id, user_followers: [...], "top_words": top_words, "top_hashtags": top_hashtag}}
# parser.hash_user(username=username, user_data={})