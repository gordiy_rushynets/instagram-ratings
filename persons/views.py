import time
import datetime
from threading import Thread

from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from .models import Person, Post, Username
from .parser import user_posts
from .parser import person as certain_person

from .parser import posts as posts_parsing
from instagramparser.instagram_parser import create_parser
from instagramparser.local_settings import login, pswd

from .utils import parse_date_day_number
from .utils import parse_time_hour

# PARSER = create_parser(login, pswd)

def periodicParser(request):
    print("POSTS PARSING STARTED")
    th_posts = Thread(target=posts_parsing)
    th_posts.start()
    return JsonResponse({"Started": "TRUE"})

@csrf_exempt
def users(request):
    p = Person.objects.all()

    now = datetime.datetime.now()
    # print (now.year, now.month, now.day, now.hour, now.minute, now.second)

    persons = []

    for person in p:
        user = {
            'id': person.id,
            'username': person.username,
            'full_name': person.full_name,
            'profile_pic_versions320': person.profile_pic_versions320,
            'profile_pic_versions640': person.profile_pic_versions640,
            'profile_pic_versions1080': person.profile_pic_versions1080,
            'media_count': person.media_count,
            'follower_count': person.follower_count,
            'following_count': person.following_count,
            'biography': person.biography,

            'count_likes': person.count_likes,
            'count_comments': person.count_comments,

            'added_user': person.added_user
        }
        persons.append(user)

    return JsonResponse({'persons': persons})

@csrf_exempt
def added_user(request):
    '''
    :param request: only POST request
    :return: json with user credentials
    '''

    PARSER = create_parser(login, pswd)

    if request.method == 'POST':
        username = request.POST['username']
        # check even if object is created before
        try:
            p = Person.objects.get(username=username)
        except:
            # try to scratch username
            try:
                certain_person(PARSER, username, True)

                p = Person.objects.get(username=username)
            except:
                data = {"Error": 'Credentials are not correct'}
                return JsonResponse(data)
    else:
        data = {'Error': "Only POST requests"}
        return JsonResponse(data)

    user_posts(parser=PARSER, username=username)

    person = {
        'id': p.id,
        'username': p.username,
        'full_name': p.full_name,
        'profile_pic_versions320': p.profile_pic_versions320,
        'profile_pic_versions640': p.profile_pic_versions640,
        'profile_pic_versions1080': p.profile_pic_versions1080,
        'media_count': p.media_count,
        'follower_count': p.follower_count,
        'following_count': p.following_count,
        'biography': p.biography,

        'count_likes': p.count_likes,
        'count_comments': p.count_comments,
        'added_user': p.added_user
    }

    data = {'person': person}
    return JsonResponse(data)

@csrf_exempt
def person_posts(request):
    '''
    Send certain person's posts from db
    :param request: username
    :return: json response
    '''

    PARSER = create_parser(login, pswd) 

    last_parsing = Username.objects.all[0].last_parsing
    now = datetime.datetime.now()

    # check parsing
    if parse_date_day_number(last_parsing) != parse_date_day_number(now) or (parse_time_hour(now) - parse_time_hour(last_parsing)) < 4:
        time.sleep(5)
        th_posts = Thread(target=posts_parsing)
        th_posts.start()

    if request.method == 'POST':
        username = request.POST['username']
        user = {}
        # check even if user is created
        try:
            p = Person.objects.get(username=username)
        except:
            # try to scratch username
            try:
                th = Thread(target=certain_person, args=[PARSER, username])
                th.start()

                user_posts(parser=PARSER, username=username)

                p = Person.objects.get(username=username)
            except:
                data = {"Error": 'Credentials are not correct'}
                return JsonResponse(data)
    else:
        data = {'Error': "Only POST requests. SEND AS key parameter - username"}
        return JsonResponse(data)

    user_posts(PARSER, username)

    p_posts = Post.objects.filter(person=p)

    info = {
        'id': p.id,
        'username': p.username,
        'full_name': p.full_name,
        'profile_pic_versions320': p.profile_pic_versions320,
        'profile_pic_versions640': p.profile_pic_versions640,
        'profile_pic_versions1080': p.profile_pic_versions1080,
        'media_count': p.media_count,
        'follower_count': p.follower_count,
        'following_count': p.following_count,
        'biography': p.biography,

        'count_likes': p.count_likes,
        'count_comments': p.count_comments,
        'added_user': p.added_user
    }

    user_publication = []

    for p_post in p_posts:
        user_post = {
            'id': p_post.id,
            'user': info,
            'post_photo_url': p_post.post_photo_url,
            'text': p_post.text,
            'like_count': p_post.like_count,
            'comment_count': p_post.comment_count,
            'created_at': p_post.created_at
        }

        user_publication.append(user_post)

    data = {'posts': user_publication}

    return JsonResponse(data)

@csrf_exempt
def all_posts(request):
    if request.method == 'GET':

        try:
            posts = Post.objects.all()
        except:
            return {'posts': []}


        new_posts = []

        for post in posts:
            info = {
                'id': post.id,
                'username': post.person.username,
                'full_name': post.person.full_name,
                'post_photo_url': post.post_photo_url,
                'text': post.text,
                'like_count': post.like_count,
                'comment_count': post.comment_count,
                'created_at': post.created_at
            }

            new_posts.append(info)

        data = {
            'posts': new_posts
        }

    elif request.method == 'POST':
        days = request.POST['days']

        posts = Post.objects.all()

        new_posts = []

        for post in posts:
            if post.was_published_recently(days):
                info = {
                    'id': post.id,
                    'username': post.person.username,
                    'full_name': post.person.full_name,
                    'post_photo_url': post.post_photo_url,
                    'text': post.text,
                    'like_count': post.like_count,
                    'comment_count': post.comment_count,
                    'created_at': post.created_at
                }

                new_posts.append(info)

        data = {
            'posts': new_posts
        }
    return JsonResponse(data)


