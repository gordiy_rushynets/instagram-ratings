import os
import xlrd
from .parser import persons

file = os.getcwd()+'/users.xlsx'

persons()

def read_exel(path_to_file):
    # To open Workbook
    wb = xlrd.open_workbook(path_to_file)
    sheet = wb.sheet_by_index(0)

    users = []

    # For row 0 and column 0
    i = 0
    while True:
        try:
            user = sheet.cell_value(i, 0)
            users.append(user)
            i += 1
        except IndexError:
            break
    return users
