import datetime
from django.db import models

from django.utils import timezone


class Username(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=150)
    full_name_at_russion = models.CharField(max_length=150)
    last_parsing = models.DateTimeField(auto_now_add=True, blank=True)
    added_user = models.BooleanField(default=False)

    def __str__(self):
        return 'Username: {0}, full name: {1}, added user: {2}'.format(self.username, self.full_name_at_russion, self.added_user)

    class Meta:
        verbose_name = "Пользователь Instagram (Для админа)"
        verbose_name_plural = "Пользователи Instagram (Для админа)"

class Person(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=150)
    full_name = models.CharField(max_length=500)
    profile_pic_versions320 = models.TextField()
    profile_pic_versions640 = models.TextField()
    profile_pic_versions1080 = models.TextField()
    media_count = models.IntegerField(default=0)
    follower_count = models.IntegerField(default=1)
    following_count = models.IntegerField(default=1)
    biography = models.TextField()

    count_likes = models.IntegerField(default=0)
    count_comments = models.IntegerField(default=0)

    added_user = models.BooleanField(default=False)

    def __str__(self):
        return 'Username: {0}, Full name: {1}'.format(self.username, self.full_name)

    class Meta:
        verbose_name = 'Человек'
        verbose_name_plural = 'Люди'

class Post(models.Model):
    id = models.AutoField(primary_key=True)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    post_photo_url = models.TextField()
    text = models.TextField()
    like_count = models.IntegerField(default=0)
    comment_count = models.IntegerField(default=0)
    created_at = models.DateTimeField()

    def __str__(self):
        return 'Username - {0}, Created - {1}, Like Count - {2}'.format(self.person.username, self.created_at, self.like_count)

    def was_published_recently(self, day):
        return self.created_at >= (timezone.now() - datetime.timedelta(days=day))

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
