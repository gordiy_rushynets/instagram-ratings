from django.contrib import admin
from .models import *


class PersonAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'full_name', 'media_count', 'follower_count', 'following_count', 'count_comments', 'count_likes', 'added_user']

    class Meta:
        model = Person

admin.site.register(Person, PersonAdmin)

admin.site.register(Post)

admin.site.register(Username)
