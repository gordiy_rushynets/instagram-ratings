import datetime
# 2019-11-02 17:42:32.673240+00:00

def parse_date_day_number(date_and_time):
    date = date_and_time.split(' ')[0]
    day_number = date.split('-')[2]

    if day_number[0] == '0':
        return int(day_number[1])
    else:
        return int(day_number)

def parse_time_hour(date_and_time):
    time = date_and_time.split(' ')[1]
    hour = time.split(':')[0]
    return int(hour)


def datetime_to_int(dt_time):
    return 10000 * dt_time.year + 100 * dt_time.month + dt_time.day
