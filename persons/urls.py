from django.urls import path
from . import views


urlpatterns = [
    path('', views.users, name='persons'),
    path('persons/posts', views.person_posts, name='person_posts'),
    path('posts/', views.all_posts, name='all_posts'),
    path('person/added_user/', views.added_user, name='added_user'),
    path('periodic_parser/', views.periodicParser, name='peridic_parser')
]