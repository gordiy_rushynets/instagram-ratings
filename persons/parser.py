from __future__ import absolute_import, unicode_literals

import time
import random
from datetime import datetime
from threading import Thread
from .models import Person, Post, Username
from .utils import datetime_to_int
from instagramparser.instagram_parser import create_parser, person_rating, person_data, get_user_posts
from instagramparser.local_settings import login, pswd


def person(parser, username, added_user):
    data = person_data(parser, username)
    rating = person_rating(parser, username)

    obj, created = Person.objects.update_or_create(username=data['username'],
                                                   full_name=data['full_name'],
                                                   profile_pic_versions320=data['profile_pic_version320'],
                                                   profile_pic_versions640=data['profile_pic_version640'],
                                                   profile_pic_versions1080=data['profile_pic_version1080'],
                                                   media_count=data['media_count'],
                                                   follower_count=data['follower_count'],
                                                   following_count=data['following_count'],
                                                   biography=data['biography'],

                                                   count_likes=rating['count_likes'],
                                                   count_comments=rating['count_comments'],
                                                   added_user=added_user)
    return data

def user_posts(parser, username):
    user = None

    try:
        user = Person.objects.get(username=username)
    except Exception as e:
        print("parser.py, post function:", e)
        # create person
        try:
            person(parser, username, False)

            user = Person.objects.get(username=username)
        except:
            print("parser.py, post function: user ({0}) is not created. Exit from function.".format(username))
            return {'status': 'fail'}

    posts = get_user_posts(parser, username)

    if posts is None:
        return {'status': 'fail'}

    for item in posts['items']:
        # create post objects

        if item['created_at'] is not None:
            time = item['created_at']
        else:
            time = datetime_to_int(datetime.now())

        created_at = datetime.fromtimestamp(time).strftime("%Y-%m-%d %I:%M:%S+00:00")

        obj, created = Post.objects.update_or_create(
            person=user,
            text=item['text'],
            like_count=item['like_count'],
            comment_count=item['comment_count'],
            created_at=created_at,
            defaults={'post_photo_url': item['post_photo_url']}
        )

        print(obj, created)


def posts():
    users = Username.objects.all()

    for user in users:
        # print(user.username)
        # user_posts(parser, user.username)

        parser = create_parser(login, pswd)

        th_posts = Thread(target=user_posts, args=[parser, user.username])
        th_posts.start()

        random_sleep = random.randint(0, 3)
        time.sleep(random_sleep)
