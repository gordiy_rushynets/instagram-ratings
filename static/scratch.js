function visual(data) {

  var card = '\n  <div class="col-md-4>\n    <div class="card mb-4 box-shadow">\n      <img class="card-img-top" src="' + data.post_photo_url + '" data-src="" alt="' + data.username + '">\n      <div class="card-body">\n        <p class="card-text">\n          <h3>' + data.full_name + '</h3>\n          <span>' + data.biography + '</span>\n          <div class="col-sm">\n            <span><b>Followers:</b> ' + data.follower_count + '</span>\n          </div>\n          <div class="col-sm">\n            <span><b>Media:</b> ' + data.media_count + '</span>\n            <span><b>Count likes:</b> ' + data.count_likes + '</span>\n            <span><b>Count comments:</b> ' + data.count_comments + '</span>\n          </div>\n        </p>\n        <div class="d-flex justify-content-between align-items-center">\n          <div class="btn-group">\n            <button type="button" class="btn btn-sm btn-outline-secondary"><a href="{% url \'person_posts\' ' + data.username + ' %}">View</a></button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>"';

  var demo = document.getElementById("row");

  demo.innerHTML = card;
}